# Budget

Dependecies:
  * Node
  * Phoenix
  * Elixir
  * postgres

To start the app:

  * Install phoenix dependencies with `mix deps.get`
  * Install react dependencies with `npm install`
  * Install webpack globally with `npm install -g webpack`
  * Compile assets with `webpack`
  * Create and migrate your database with `mix ecto.create && mix ecto.migrate`
  * Create some seed data in database with `mix run priv/repo/seeds.exs`
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.
