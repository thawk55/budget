defmodule Budget.Repo.Migrations.CreateExpense do
  use Ecto.Migration

  def change do
    create table(:expenses) do
      add :name, :string, null: false
      add :category_id, :integer, null: false

      timestamps()
    end

  end
end
