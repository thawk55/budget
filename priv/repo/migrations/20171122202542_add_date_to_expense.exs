defmodule Budget.Repo.Migrations.CreateExpense do
  use Ecto.Migration

  def change do
    alter table(:expenses) do
      add :date, :date, null: false
    end
  end
end
