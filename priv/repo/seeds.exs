# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Budget.Repo.insert!(%Budget.SomeModel{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will halt execution if something goes wrong.
Budget.Repo.insert!(%Budget.Category{"name": "Food"})
Budget.Repo.insert!(%Budget.Category{"name": "House"})
Budget.Repo.insert!(%Budget.Category{"name": "Miscellaneous"})
Budget.Repo.insert!(%Budget.Expense{"name": "Lunch", "category_id": 1, "cost": -12.17, "date": Ecto.Date.cast!("2017-11-26")})
Budget.Repo.insert!(%Budget.Expense{"name": "Dinner", "category_id": 1, "cost": -7.50, "date": Ecto.Date.cast!("2017-11-26")})
Budget.Repo.insert!(%Budget.Expense{"name": "Breakfast", "category_id": 1, "cost": -5.82, "date": Ecto.Date.cast!("2017-11-27")})
