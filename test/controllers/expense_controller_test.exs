defmodule Budget.ExpenseControllerTest do
  use Budget.ConnCase

  alias Budget.Expense
  @valid_attrs %{category_id: 42, name: "some name"}
  @invalid_attrs %{}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, expense_path(conn, :index)
    assert json_response(conn, 200)["data"] == []
  end

  test "shows chosen resource", %{conn: conn} do
    expense = Repo.insert! %Expense{}
    conn = get conn, expense_path(conn, :show, expense)
    assert json_response(conn, 200)["data"] == %{"id" => expense.id,
      "name" => expense.name,
      "category_id" => expense.category_id}
  end

  test "renders page not found when id is nonexistent", %{conn: conn} do
    assert_error_sent 404, fn ->
      get conn, expense_path(conn, :show, -1)
    end
  end

  test "creates and renders resource when data is valid", %{conn: conn} do
    conn = post conn, expense_path(conn, :create), expense: @valid_attrs
    assert json_response(conn, 201)["data"]["id"]
    assert Repo.get_by(Expense, @valid_attrs)
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, expense_path(conn, :create), expense: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "updates and renders chosen resource when data is valid", %{conn: conn} do
    expense = Repo.insert! %Expense{}
    conn = put conn, expense_path(conn, :update, expense), expense: @valid_attrs
    assert json_response(conn, 200)["data"]["id"]
    assert Repo.get_by(Expense, @valid_attrs)
  end

  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
    expense = Repo.insert! %Expense{}
    conn = put conn, expense_path(conn, :update, expense), expense: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "deletes chosen resource", %{conn: conn} do
    expense = Repo.insert! %Expense{}
    conn = delete conn, expense_path(conn, :delete, expense)
    assert response(conn, 204)
    refute Repo.get(Expense, expense.id)
  end
end
