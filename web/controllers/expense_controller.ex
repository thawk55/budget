defmodule Budget.ExpenseController do
  use Budget.Web, :controller

  alias Budget.Expense

  def index(conn, _params) do
    expenses = Repo.all(Expense)
    render(conn, "index.json", expenses: expenses)
  end

  def create(conn, expense_params) do
    changeset = Expense.changeset(%Expense{}, expense_params)

    case Repo.insert(changeset) do
      {:ok, expense} ->
        conn
        |> put_status(:created)
        |> put_resp_header("location", expense_path(conn, :show, expense))
        |> render("show.json", expense: expense)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Budget.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    expense = Repo.get!(Expense, id)
    render(conn, "show.json", expense: expense)
  end

  def update(conn, %{"id" => id, "name" => name, "cost" => cost, "category_id" => category_id, "date" => date}) do
    expense = Repo.get!(Expense, id)
    changeset = Expense.changeset(expense, %{"name" => name, "cost" => cost, "category_id" => category_id, "date" => date })

    case Repo.update(changeset) do
      {:ok, expense} ->
        render(conn, "show.json", expense: expense)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Budget.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    expense = Repo.get!(Expense, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(expense)

    send_resp(conn, :no_content, "")
  end
end
