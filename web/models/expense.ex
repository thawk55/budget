defmodule Budget.Expense do
  use Budget.Web, :model

  schema "expenses" do
    field :name, :string
    field :category_id, :integer
    field :cost, :float
    field :date, :date

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name, :category_id, :cost, :date])
    |> validate_required([:name, :category_id, :cost, :date])
  end
end
