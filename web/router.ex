defmodule Budget.Router do
  use Budget.Web, :router

  pipeline :web do 
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", Budget do
    pipe_through :api

    post "/categories", CategoryController, :create
    get "/categories", CategoryController, :index
    get "/categories/:id", CategoryController, :show
    put "/categories/:id", CategoryController, :update    
    delete "/categories/:id", CategoryController, :delete

    post "/expenses", ExpenseController, :create
    get "/expenses", ExpenseController, :index
    get "/expenses/:id", ExpenseController, :show
    put "/expenses/:id", ExpenseController, :update    
    delete "/expenses/:id", ExpenseController, :delete
  end

  scope "/", Budget do
    pipe_through :web
    get "/*path", PageController, :index
  end
end
