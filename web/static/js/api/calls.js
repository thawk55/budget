import q from 'q';

export function makeCall(url, method, headers) {
    var defer = q.defer();
    handleResponse(fetch(url, {
        method: method || 'GET',
        headers: headers || {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
    }), defer);
    return defer.promise;
}

export function makeCallWithBody(url, method, headers, body) {
    var defer = q.defer();
    handleResponse(fetch(url, {
        method: method || 'POST',
        headers: headers || {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(body)
    }), defer);
    return defer.promise;
}

function handleResponse(req, defer) {
    req.then(results => {
        if (results.status == 204){
            return null;
        }
        return results.json();
    })
    .then((data) => {
        if(data){
            defer.resolve(data.data);
        } else {
            defer.resolve();
        }
    })
    .catch(e => {
        defer.reject(e);
    })
}