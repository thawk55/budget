import { makeCall, makeCallWithBody } from './calls';

export function getCategoriesFromAPI() {
    return makeCall('/api/categories');
}

export function createCategoriesInAPI(category) {
    return makeCallWithBody('/api/categories', 'POST', null, category);
}

export function updateCategoriesInAPI(category) {
    return makeCallWithBody('/api/categories/' + category.id, 'PUT', null, category);
}

export function getCategoryFromAPI(id) {
    return makeCall('/api/categories/' + id);
}

export function deleteCategoryInAPI(id) {
    return makeCall('/api/categories/' + id, 'DELETE');
}

