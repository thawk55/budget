import { makeCall, makeCallWithBody } from './calls';

export function getExpensesFromAPI() {
    return makeCall('/api/expenses');
}

export function createExpenseInAPI(expense) {
    return makeCallWithBody('/api/expenses', 'POST', null, expense);
}

export function updateExpenseInAPI(expense) {
    return makeCallWithBody('/api/expenses/' + expense.id, 'PUT', null, expense);
}

export function getExpenseFromAPI(id) {
    return makeCall('/api/expenses/' + id);
}

export function deleteExpenseInAPI(id) {
    return makeCall('/api/expenses/' + id, 'DELETE');
}

