import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {Modal, Button} from 'react-bootstrap';

class AddModal extends Component {
  constructor(props){
    super(props);

    this.state = {open: false};
  }

  render(props){
    let closeModal = () => this.setState({ open: false });

    let saveAndClose = () => {
      this.props.save();
      closeModal();
    }

    let openModal = () => {
      this.setState({open: true});
    }

    return (
      <div className="static-modal">
        <Button bsStyle="primary" bsSize="large" onClick={openModal}>
          <span className='glyphicon glyphicon-plus'/>
        </Button>
        <Modal show={this.state.open} onHide={closeModal}>
          <Modal.Header>
            <Modal.Title>{this.props.title}</Modal.Title>
          </Modal.Header>
          <Modal.Body>{this.props.children}</Modal.Body>
          <Modal.Footer>
            <Button onClick={closeModal}>Close</Button>
            <Button bsStyle="primary" onClick={saveAndClose}>Save</Button>
          </Modal.Footer>
        </Modal>
      </div>
    )
  }
}

export default AddModal;