import React, { Component, PropTypes } from 'react';
import { deleteCategoryInAPI } from '../api/categoryCalls';

export default class Category extends Component {
  constructor(props){
    super(props);

    this.state = {open: false};
  }

  render() {

    let deleteCategory = event => {
      let { category, refreshFunction } = this.props;
      if(category.id) {
        deleteCategoryInAPI(category.id).then(refreshFunction);
      }
    }
  
    let editCategory = () => {
      this.props.editCategory(this.props.category);   
    }

    const category = this.props.category;
    return (
      <tr>
        <td>{category.name}</td>
        <td>
          <span onClick={editCategory} className="glyphicon glyphicon-pencil"/>&nbsp;
          <span onClick={deleteCategory} className="glyphicon glyphicon-remove"/>
        </td>
      </tr>
    )
  }
}

Category.propTypes = {
  category: PropTypes.object.isRequired,
  refreshFunction: PropTypes.func.isRequired,
  editCategory: PropTypes.func.isRequired
};