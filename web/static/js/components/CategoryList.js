import React, { Component } from 'react';
import { Table } from 'react-bootstrap';

import Category from './Category';
import SaveCategory from './SaveCategory';
import EditCategory from './EditCategory';

import { getCategoriesFromAPI } from '../api/categoryCalls';

class CategoryList extends Component {
  constructor(props) {
    super(props);

    this.state = {categories: [], openEdit: false, editCategory: {}};
  }

  componentDidMount() {
    this.getCategories();
  }

  getCategories() {
    getCategoriesFromAPI().then(categories => this.setState({categories}));
  }
  
  render() {
    let getCat = () => {
      this.getCategories();
    }

    let editCat = (category) => {
      this.setState({openEdit: true, editCategory: category});
    }
    
    let closeEdit = () => {
      this.setState({openEdit: false, editCategory: {}});
    }

    const categories = this.state.categories.map((item, i) => (
      <Category key={i} category={item} refreshFunction={getCat} editCategory={editCat}/>
    ));

    let cat = this.state.editCategory;
  
    return (
      <div>
        <SaveCategory savedAction={getCat}/>
        <EditCategory open={this.state.openEdit} updatedAction={getCat} category={cat} close={closeEdit}/>
        <Table responsive>
          <thead>
            <tr>
              <th scope='col'>Category</th>
              {/* For Controls */}
              <th scope='col'></th>
            </tr>
          </thead>
          <tbody>
            {categories}
          </tbody>
        </Table>
      </div>
    );
  }

}

export default CategoryList