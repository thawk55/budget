import React, { Component, PropTypes } from "react";
import EditModal from './EditModal';
import { FormGroup, ControlLabel, FormControl, HelpBlock } from 'react-bootstrap';
import { updateCategoriesInAPI } from '../api/categoryCalls';

export default class EditCategory extends Component {
  constructor(props) {
    super(props);
    this.state = {categoryName: props.category.name || ''};
  }

  componentWillReceiveProps(nextProps) {
    this.state = {categoryName: nextProps.category.name || ''};
  }    

  render(){

    let updateCategory = () => {
      let category = {
        name: this.state.categoryName, 
        id: this.props.category.id
      }
      updateCategoriesInAPI(category).then(() => this.props.updatedAction());
      this.props.close();
    }
  
    let updateCategoryName = event => {
      this.setState({categoryName: event.target.value});
    }
    

    return (
      <EditModal 
       title='Edit Category' 
       close={this.props.close}
       save={updateCategory} 
       showButton={false} 
       show={this.props.open}>
        <FormGroup controlId='addName'>
          <ControlLabel>Name</ControlLabel>
          <FormControl type='text' value={this.state.categoryName} placeholder='Category Name' onChange={updateCategoryName}/>
          <FormControl.Feedback />
          <HelpBlock>Name is required and must be unique</HelpBlock>
        </FormGroup>
      </EditModal>
    )
  }
}

EditCategory.defaultProps = {
  open: false
}

EditCategory.propTypes = {
  updatedAction: PropTypes.func,
  category: PropTypes.object.isRequired,
  open: PropTypes.bool,
  close: PropTypes.func.isRequired,
};