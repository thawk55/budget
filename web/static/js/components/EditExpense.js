import React, { Component, PropTypes } from "react";
import { Dropdown, MenuItem, FormGroup, ControlLabel, FormControl, HelpBlock } from 'react-bootstrap'
import moment from 'moment';

import EditModal from './EditModal';
import { updateExpenseInAPI } from '../api/expenseCalls';

export default class EditExpense extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: props.expense.name || '',
      cost: props.expense.cost || 0,
      category_id: props.expense.category_id || 0,
      categoryName: 'Categories',
      date: props.expense.date || moment().format('YYYY-MM-DD'),
    };

    this.updateExpense = this.updateExpense.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    const category = nextProps.categories.find((cat) => {
      if (cat.id == nextProps.expense.category_id) {
        return cat;
      } 
    });

    const categoryName = !category ? 'Categories' : category.name

    this.state = {
      name: nextProps.expense.name || '',
      cost: nextProps.expense.cost || 0,
      category_id: nextProps.expense.category_id || 0,
      categoryName: categoryName,
      date: nextProps.expense.date,
    };
  }    

  updateName(event){
    this.setState({name: event.target.value});
  }

  updateCost(event){
    this.setState({cost: event.target.value});
  }

  updateCategory(category){
    this.setState({category_id: category.id, categoryName: category.name});
  }

  updateDate(event){
    this.setState({date: event.target.value});
  }

  updateExpense() {
    let expense = {
      name: this.state.name, 
      id: this.props.expense.id,
      cost: this.state.cost,
      category_id: this.state.category_id,
      date: this.state.date,
    }
    updateExpenseInAPI(expense).then(() => this.props.updatedAction());
    this.props.close();
  }


  render(){
    let items = this.props.categories.map(c => {
      return <MenuItem key={c.id} eventKey={c.id} onClick={() => this.updateCategory(c)}>{c.name}</MenuItem>
    });
  
    return (
      <EditModal 
       title='Edit Expense' 
       close={this.props.close}
       save={this.updateExpense} 
       showButton={false} 
       show={this.props.open}>
        <form>
          <FormGroup controlId='addName'>
            <ControlLabel>Name</ControlLabel>
            <FormControl type='text' value={this.state.name} placeholder='Expense Name or description' onChange={evt => this.updateName(evt)}/>
            <FormControl.Feedback />
            <HelpBlock>Name is required</HelpBlock>
          </FormGroup>
          <FormGroup controlId='addCost'>
            <ControlLabel>Cost</ControlLabel>
            <FormControl type='number' value={this.state.cost} onChange={evt => this.updateCost(evt)}/>
            <FormControl.Feedback />
            <HelpBlock>Cost is required and cannot be 0</HelpBlock>
          </FormGroup>
          <FormGroup controlId='addDate'>
            <ControlLabel>Date</ControlLabel>
            <FormControl type='date' value={this.state.date} onChange={evt => this.updateDate(evt)}/>
          </FormGroup>
          <FormGroup>
            <Dropdown id='Category Dropdown'>
              <Dropdown.Toggle>
                {this.state.categoryName}
              </Dropdown.Toggle>
              <Dropdown.Menu title='Categories' id='categories-dropdown'>
                {items}
              </Dropdown.Menu>
            </Dropdown>
          </FormGroup>
        </form>
      </EditModal>
    )
  }
}

EditExpense.defaultProps = {
  open: false
}

EditExpense.propTypes = {
  updatedAction: PropTypes.func,
  expense: PropTypes.object.isRequired,
  categories: PropTypes.array.isRequired,
  open: PropTypes.bool,
  close: PropTypes.func.isRequired,
};