import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import {Modal, Button} from 'react-bootstrap';

export default class EditModal extends Component {

  render(props){
    return (
      <div className="static-modal">
        <Modal show={this.props.show} onHide={this.props.close}>
          <Modal.Header>
            <Modal.Title>{this.props.title}</Modal.Title>
          </Modal.Header>
          <Modal.Body>{this.props.children}</Modal.Body>
          <Modal.Footer>
            <Button onClick={this.props.close}>Close</Button>
            <Button bsStyle="primary" onClick={this.props.save}>Save changes</Button>
          </Modal.Footer>
        </Modal>
      </div>
    )
  }
}

EditModal.propTypes = {
  show: PropTypes.bool.isRequired,
  close: PropTypes.func.isRequired,
  save: PropTypes.func.isRequired,
};