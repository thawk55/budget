import React, { Component, PropTypes } from 'react';
import numbro from 'numbro';
import { deleteExpenseInAPI } from '../api/expenseCalls';

export default class Expense extends Component {
  render() {
    let deleteExpense = event => {
      let { expense, refreshFunction } = this.props;
      if(expense.id) {
        deleteExpenseInAPI(expense.id).then(refreshFunction);
      }
    }
  
    let editExpense = () => {
      this.props.editExpense(this.props.expense);   
    }

    const {expense, categories} = this.props;
    if (!expense || !categories) {
      return;
    }
    const category = categories.find((cat) => {
      if (cat.id == expense.category_id) {
        return cat;
      } 
    });

    const categoryName = !category ? 'None' : category.name
    
    return (
      <tr>
        <td>{expense.name}</td>
        <td>{numbro(expense.cost).format('$0,0.00')}</td>
        <td>{expense.date}</td>
        <td>{categoryName}</td>
        <td>
          <span onClick={editExpense} className='glyphicon glyphicon-pencil'/>&nbsp;
          <span onClick={deleteExpense} className="glyphicon glyphicon-remove"/>
        </td>
      </tr>
    )
  }
}

Expense.propTypes = {
  expense: PropTypes.object.isRequired,
  categories: PropTypes.array.isRequired,
  refreshFunction: PropTypes.func.isRequired,
  editExpense: PropTypes.func.isRequired
};