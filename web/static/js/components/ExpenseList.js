import React, { Component } from 'react';
import { Table } from 'react-bootstrap';
import numbro from 'numbro';
import Expense from './Expense';
import SaveExpense from './SaveExpense';
import EditExpense from './EditExpense';

import { getCategoriesFromAPI } from '../api/categoryCalls';
import { getExpensesFromAPI } from '../api/expenseCalls';

class ExpenseList extends Component {
  constructor(props) {
    super(props);

    this.state = {expenses: [], categories: [], openEdit: false, editExpense: {}};
  }

  componentDidMount() {
    this.getExpenses();
    this.getCategories();
  }

  getExpenses() {
    fetch('/api/expenses')
      .then(results => { return results.json() })
      .then(({data}) => {
        this.setState({ expenses: data });
      });
  }

  getCategories() {
    getCategoriesFromAPI().then(categories => this.setState({categories}));
  }

  render() {
    let getExp = () => {
      this.getExpenses();
    }

    let editExpense = (expense) => {
      this.setState({openEdit: true, editExpense: expense});
    }
    
    let closeEdit = () => {
      this.setState({openEdit: false, editExpense: {}});
    }

    let sum = 0;

    const expenses = this.state.expenses.map((item, i) => {
      sum += item.cost;
      return <Expense key={i} expense={item} categories={this.state.categories} refreshFunction={getExp} editExpense={editExpense}/>
    });

    let exp = this.state.editExpense;

    return (
      <div>
        <SaveExpense savedAction={this.getExpenses.bind(this)} categories={this.state.categories}/>
        <EditExpense open={this.state.openEdit} updatedAction={this.getExpenses.bind(this)} categories={this.state.categories} expense={exp} close={closeEdit}/>
        <Table striped responsive>
          <thead>
            <tr>
              <th scope="col">Description</th>
              <th scope="col">Cost</th>
              <th scope="col">Date</th>
              <th scope="col">Category</th>
              <th scope="col"></th>
            </tr>
          </thead>
          <tbody>
            {expenses}
            <tr>
              <th scope="row">Total</th>
              <th scope="row">{numbro(sum).format('$0,0.00')}</th>
              <th scope="row"></th>
              <th scope="row"></th>
              <th scope="row"></th>
            </tr>
          </tbody>
        </Table>
      </div>
    );
  }

}

export default ExpenseList