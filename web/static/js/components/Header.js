import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router-dom';
import { Nav, NavItem } from 'react-bootstrap';
import { withRouter } from 'react-router-dom'

class Header extends Component {
  constructor(props){
    super(props);
    this.state = {activeKey: '1'}
  }

  render() {
    let handleSelect = (eventKey) => {
      this.setState({activeKey: eventKey});
    }

    let handleClick = (e, route) => {
      this.props.history.push(route); 
    }

    return (
      <div className='jumbotron'>
        <h2>Phoenix Budget</h2>
        <Nav bsStyle='pills' activeKey={this.state.activeKey} onSelect={handleSelect}>
          <NavItem eventKey='1' href='/expenses' onClick={(e) => handleClick(e, '/expenses')}>Expenses</NavItem>
          <NavItem eventKey='2' href='/categories' onClick={(e) => handleClick(e, '/categories')}>Categories</NavItem>
        </Nav>
      </div>
    )
  }
}

Header.propTypes = {
  history: PropTypes.object.isRequired
}

export default withRouter(Header);
