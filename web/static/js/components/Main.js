import React, { Component } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom'
import CategoryList from "./CategoryList";
import ExpenseList from "./ExpenseList";


const Main = () => (
    <main>
        <Switch>
            <Route path='/categories' component={CategoryList}/>
            <Route path='/expenses' component={ExpenseList}/>
            <Redirect from='/' to="/expenses"/>
        </Switch>
    </main>
)

export default Main
