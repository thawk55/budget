import React, { Component, PropTypes } from "react";
import { FormGroup, ControlLabel, FormControl, HelpBlock } from 'react-bootstrap';
import AddModal from './AddModal';
import { createCategoriesInAPI } from '../api/categoryCalls';

export default class SaveCategory extends Component {
  constructor(props) {
    super(props);
  
    this.state = {newCategory: ''};
  }

  saveCategory(){
    if(this.state.newCategory != ''){
        createCategoriesInAPI({name: this.state.newCategory}).then(() => this.props.savedAction());
        this.setState({newCategory: ''});
    }
  }

  updateCategory(event){
      this.setState({newCategory: event.target.value})
  }

  render(){
    return (
      <AddModal title='Add Category' save={this.saveCategory.bind(this)}>
        <form>
          <FormGroup controlId='addName'>
            <ControlLabel>Name</ControlLabel>
            <FormControl type='text' value={this.state.newCategory} placeholder='Category Name' onChange={evt => this.updateCategory(evt)}/>
            <FormControl.Feedback />
            <HelpBlock>Name is required and must be unique</HelpBlock>
          </FormGroup>
        </form>
      </AddModal>
    )
  }
}

SaveCategory.propTypes = {
  savedAction: PropTypes.func
};