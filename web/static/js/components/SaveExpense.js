import React, { Component, PropTypes } from 'react';
import { Dropdown, MenuItem, FormGroup, ControlLabel, FormControl, HelpBlock } from 'react-bootstrap'
import AddModal from './AddModal';
import moment from 'moment';

import { createExpenseInAPI } from '../api/expenseCalls';

export default class SaveCategory extends Component {
  constructor(props) {
    super(props);
  
    this.state = {
      name: '',
      cost: 0,
      category_id: 0,
      categoryName: 'Categories',
      date: moment().format('YYYY-MM-DD'),
    };
  }

  saveExpense(){
    if(this.state.name != '' && this.state.cost != 0 && this.state.category_id != 0 && this.state.date){
      let expense = {
        name: this.state.name,
        cost: this.state.cost,
        category_id: this.state.category_id,
        date: this.state.date
      }
      createExpenseInAPI(expense).then(() => this.props.savedAction());
      this.setState({
        name: '',
        cost: 0,
        category_id: 0,
        categoryName: 'Categories',
        date: moment().format('YYYY-MM-DD'),
      });
    }
  }

  updateName(event){
      this.setState({name: event.target.value});
  }

  updateCost(event){
    this.setState({cost: event.target.value});
  }

  updateCategory(category){
    this.setState({category_id: category.id, categoryName: category.name});
  }

  updateDate(event){
    this.setState({date: event.target.value});
  }

  render(){
    let items = this.props.categories.map((c, i) => (
      <MenuItem key={c.id} eventKey={c.id} onClick={() => this.updateCategory(c)}>{c.name}</MenuItem>
    ));

    return (
      <AddModal title='Add Expense' save={this.saveExpense.bind(this)}>
        <form>
          <FormGroup controlId='addName'>
            <ControlLabel>Name</ControlLabel>
            <FormControl type='text' value={this.state.name} placeholder='Expense Name or description' onChange={evt => this.updateName(evt)}/>
            <FormControl.Feedback />
            <HelpBlock>Name is required</HelpBlock>
          </FormGroup>
          <FormGroup controlId='addCost'>
            <ControlLabel>Cost</ControlLabel>
            <FormControl type='number' value={this.state.cost} onChange={evt => this.updateCost(evt)}/>
            <FormControl.Feedback />
            <HelpBlock>Cost is required and cannot be 0</HelpBlock>
          </FormGroup>
          <FormGroup controlId='addDate'>
            <ControlLabel>Date</ControlLabel>
            <FormControl type='date' value={this.state.date} onChange={evt => this.updateDate(evt)}/>
          </FormGroup>
          <FormGroup>
            <Dropdown id='Category Dropdown'>
              <Dropdown.Toggle>
                {this.state.categoryName}
              </Dropdown.Toggle>
              <Dropdown.Menu title='Categories' id='categories-dropdown'>
                {items}
              </Dropdown.Menu>
            </Dropdown>
          </FormGroup>
        </form>
      </AddModal>
    )
  }
}

SaveCategory.propTypes = {
  savedAction: PropTypes.func,
  categories: PropTypes.array.isRequired,
};