import React, { Component } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom'
import Main from "../components/Main";
import Header from "../components/Header";

const App = () => (
  <div>
    <Header />
    <Main />
  </div>
)

export default App
