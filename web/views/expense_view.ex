defmodule Budget.ExpenseView do
  use Budget.Web, :view

  def render("index.json", %{expenses: expenses}) do
    %{data: render_many(expenses, Budget.ExpenseView, "expense.json")}
  end

  def render("show.json", %{expense: expense}) do
    %{data: render_one(expense, Budget.ExpenseView, "expense.json")}
  end

  def render("expense.json", %{expense: expense}) do
    %{id: expense.id,
      name: expense.name,
      category_id: expense.category_id,
      cost: expense.cost,
      date: expense.date}
  end
end
